#!/usr/bin/env bash
echo "starting test-integration"

set -e 

docker_file=./env-test-integration-old/docker-compose.yaml
project_name=swiss_remit_monolith_test
back_suffix=_back_1

chmod +x ./env-test-integration-old/get-all-dirs-with-i-test.sh
chmod +x ./env-test-integration-old/start.sh

# shellcheck disable=SC2015
docker-compose -f $docker_file -p $project_name up -d \
 && echo "waiting for test container to start..." \
 && docker attach --no-stdin $project_name$back_suffix \
 || { docker-compose -f $docker_file -p $project_name down ; exit 1 ; }

docker-compose -f $docker_file -p $project_name down -v
