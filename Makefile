build:
	if [ ! -x ./build.sh ] ; then \
		chmod +x ./build.sh ; \
	fi ; \
	./build.sh

test:
	go test $$(go list ./... | grep -v /restapi/)

#for CI/CD ("clean cache")
test-all:
	go clean -testcache && go test $$(go list ./... | grep -v /restapi/)

test-i:
	if [ ! -x ./test-integration.sh ] ; then \
		chmod +x ./test-integration.sh ; \
	fi ; \
	./test-integration.sh

swagger-validate:
	if [ ! -x ./swagger-validate.sh ] ; then \
		chmod +x ./swagger-validate.sh ; \
	fi ; \
	./swagger-validate.sh

swagger-flatten:
	if [ ! -x ./swagger-flatten.sh ] ; then \
		chmod +x ./swagger-flatten.sh ; \
	fi ; \
	./swagger-flatten.sh
