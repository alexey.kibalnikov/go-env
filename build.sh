#!/bin/bash

if ! [ -x "$(command -v go)" ]; then
  echo 'Error: "go" is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
  echo 'Error: "docker" is not installed.' >&2
  exit 1
fi

set -x -e -o pipefail

# Swagger API generate
shopt -s expand_aliases
# shellcheck disable=SC2139
alias swagger="docker run --rm -it -e GOPATH=$HOME/go:/go -v $HOME:$HOME -w $(pwd) quay.io/goswagger/swagger"
swagger flatten ./swagger-base.yaml --output=swagger.yaml --format=yaml
swagger generate server -t ./internal/api/restapi -f ./swagger.yaml --exclude-main
swagger generate client -t ./internal/api/restapi -f ./swagger.yaml
sudo chown -R "$USER:$USER" ./internal/api/restapi/*

# Mock generate
go get github.com/golang/mock/mockgen
# shellcheck disable=SC2155
export PATH=$PATH:$(go env GOPATH)/bin
mockgen -source=./internal/app/app.go -destination=./internal/app/app_mock.generated.go -package=app
mockgen -source=./internal/app/app_custom.go -destination=./internal/app/app_custom_mock.generated.go -package=app
mockgen -source=./internal/app/rules_set.go -destination=./internal/app/rules_set_mock.generated.go -package=app
mockgen -source=./internal/api/auth.go -destination=./internal/api/auth_mock.generated.go -package=api

# shellcheck disable=SC1035
if !(grep -q 'insteadOf = https://github.com/' ~/.gitconfig) || !(grep -q 'url "git@github.com:"' ~/.gitconfig)
then
    git config --global --add url."git@github.com:".insteadOf "https://github.com/"
    echo "Git: will now use SSH URL instead of HTTPS for github.com"
fi

export GOSUMDB=off

go get ./...
go fmt ./...

rm -rf bin/
mkdir bin/

go build -o ./bin/monolith ./cmd/main/*

go mod tidy
