#!/bin/bash

if ! [ -x "$(command -v go)" ]; then
  echo 'Error: "go" is not installed.' >&2
  exit 1
fi

if ! [ -x "$(command -v docker)" ]; then
  echo 'Error: "docker" is not installed.' >&2
  exit 1
fi

set -x -e -o pipefail

# Swagger API generate
shopt -s expand_aliases
# shellcheck disable=SC2139
alias swagger="docker run --rm -it -e GOPATH=$HOME/go:/go -v $HOME:$HOME -w $(pwd) quay.io/goswagger/swagger"
swagger flatten ./swagger-base.yaml --output=swagger.yaml --format=yaml
